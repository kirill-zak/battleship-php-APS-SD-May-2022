<?php

namespace App;

use Battleship\Color;
use Battleship\Message;

class Console
{
    public function resetForegroundColor(): void
    {
        echo(Color::DEFAULT_GREY);
    }

    public function setForegroundColor($color): void
    {
        echo($color);
    }

    public function println($line = ""): void
    {
        echo "$line\n";
    }

    public function printMessageLn($line = ''): void
    {
        $this->printColorLn(Color::GREEN, $line);
    }

    public function printHit(): void
    {
        $this->printColorLn(Color::RED, Message::HIT);
    }

    public function printMiss(): void
    {
        $this->printColorLn(Color::CADET_BLUE, Message::MISS);
    }

    public function printDividerLine(): void
    {
        $this->println(Message::DIVIDER);
    }

    private function printColorLn(string $color, string $line = ''): void
    {
        $this->setForegroundColor($color);
        $this->println($line);
        $this->resetForegroundColor();
    }
}
