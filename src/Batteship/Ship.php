<?php

namespace Battleship;

class Ship
{
    private $name;
    private $size;
    private $color;
    private $positions = [];
    private $hitList = [];

    public function __construct($name, $size, $color = null)
    {
        $this->name = $name;
        $this->size = $size;
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    public function addPosition($input): void
    {
        $letter = substr($input, 0, 1);
        $number = substr($input, 1, 1);

        $this->positions[] = new Position($letter, $number);
    }

    public function addHit($input): void
    {
        $letter = substr($input, 0, 1);
        $number = substr($input, 1, 1);

        $this->hitList[] = new Position($letter, $number);
    }

    public function &getPositions()
    {
        return $this->positions;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function isAlive(): bool
    {
        return count(array_diff($this->positions, $this->hitList)) > 0;
    }
}
