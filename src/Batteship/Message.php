<?php

declare(strict_types=1);

namespace Battleship;

final class Message
{
    public const HIT = 'Yeah ! Nice hit !';
    public const MISS = 'Miss';
    public const DIVIDER = '-----------------------------';
}
