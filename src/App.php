<?php

namespace App;

use Battleship\GameController;
use Battleship\Position;
use Battleship\Letter;
use Battleship\Color;
use Exception;

class App
{
    private static $myFleet = array();
    private static $enemyFleet = array();
    private static $console;

    public static function run(): void
    {
        self::$console = new Console();
        self::$console->setForegroundColor(Color::MAGENTA);

        self::$console->println("                                     |__");
        self::$console->println("                                     |\\/");
        self::$console->println("                                     ---");
        self::$console->println("                                     / | [");
        self::$console->println("                              !      | |||");
        self::$console->println("                            _/|     _/|-++'");
        self::$console->println("                        +  +--|    |--|--|_ |-");
        self::$console->println("                     { /|__|  |/\\__|  |--- |||__/");
        self::$console->println("                    +---------------___[}-_===_.'____                 /\\");
        self::$console->println("                ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        self::$console->println(" __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        self::$console->println("|                        Welcome to Battleship                         BB-61/");
        self::$console->println(" \\_________________________________________________________________________|");
        self::$console->println();
        self::$console->resetForegroundColor();
        self::initializeGame();
        self::startGame();
    }

    public static function initializeEnemyFleet(): void
    {
        self::$enemyFleet = GameController::initializeShips();

        self::$enemyFleet[0]->getPositions()[] = new Position('B', 4);
        self::$enemyFleet[0]->getPositions()[] = new Position('B', 5);
        self::$enemyFleet[0]->getPositions()[] = new Position('B', 6);
        self::$enemyFleet[0]->getPositions()[] = new Position('B', 7);
        self::$enemyFleet[0]->getPositions()[] = new Position('B', 8);

        self::$enemyFleet[1]->getPositions()[] = new Position('E', 6);
        self::$enemyFleet[1]->getPositions()[] = new Position('E', 7);
        self::$enemyFleet[1]->getPositions()[] = new Position('E', 8);
        self::$enemyFleet[1]->getPositions()[] = new Position('E', 9);

        self::$enemyFleet[2]->getPositions()[] = new Position('A', 3);
        self::$enemyFleet[2]->getPositions()[] = new Position('B', 3);
        self::$enemyFleet[2]->getPositions()[] = new Position('C', 3);

        self::$enemyFleet[3]->getPositions()[] = new Position('F', 8);
        self::$enemyFleet[3]->getPositions()[] = new Position('G', 8);
        self::$enemyFleet[3]->getPositions()[] = new Position('H', 8);

        self::$enemyFleet[4]->getPositions()[] = new Position('C', 5);
        self::$enemyFleet[4]->getPositions()[] = new Position('C', 6);
    }

    /**
     * @throws Exception
     */
    public static function getRandomPosition(): Position
    {
        $rows = 8;
        $lines = 8;

        $letter = Letter::value(random_int(0, $lines - 1));
        $number = random_int(0, $rows - 1);

        return new Position($letter, $number);
    }

    public static function initializeMyFleet(): void
    {
        self::$myFleet = GameController::initializeShips();

        self::$console->printDividerLine();
        self::$console->println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        foreach (self::$myFleet as $ship) {
            self::$console->printDividerLine();
            self::$console->println();
            self::$console->printMessageLn(
                sprintf("Please enter the positions for the %s (size: %s)", $ship->getName(), $ship->getSize())
            );

            for ($i = 1; $i <= $ship->getSize(); $i++) {
                printf("\nEnter position %s of %s (i.e A3):", $i, $ship->getSize());
                $input = readline("");
                $ship->addPosition($input);
            }
        }
    }

    public static function beep(): void
    {
        echo "\007";
    }

    public static function initializeGame(): void
    {
        self::initializeMyFleet();
        self::initializeEnemyFleet();
    }

    public static function startGame(): void
    {
        self::$console->printDividerLine();
        self::$console->println("\033[2J\033[;H");
        self::$console->println("                  __");
        self::$console->println("                 /  \\");
        self::$console->println("           .-.  |    |");
        self::$console->println("   *    _.-'  \\  \\__/");
        self::$console->println("    \\.-'       \\");
        self::$console->println("   /          _/");
        self::$console->println("  |      _  /\" \"");
        self::$console->println("  |     /_\'");
        self::$console->println("   \\    \\_/");
        self::$console->println("    \" \"\" \"\" \"\" \"");

        while (true) {
            self::$console->println("");

            self::$console->printDividerLine();
            self::$console->println("Player, it's your turn. You can fire or end of game");

            self::$console->println("");
            self::$console->println('List of dead ships');
            $deadSheepList = GameController::getDeadShipList(self::$enemyFleet);

            foreach ($deadSheepList as $sheep) {
                self::$console->println(
                    sprintf('Dead sheep name [%s] ans size [%s]', $sheep->getName(), $sheep->getSize())
                );
            }

            self::$console->printDividerLine();

            self::$console->println("");
            self::$console->println('List of alive ships');
            $deadSheepList = GameController::getAliveShipList(self::$enemyFleet);

            foreach ($deadSheepList as $sheep) {
                self::$console->println(
                    sprintf('Alive sheep name [%s] ans size [%s]', $sheep->getName(), $sheep->getSize())
                );
            }


            self::$console->printDividerLine();
            self::$console->printMessageLn("Enter coordinates for your shot :");

            $position = readline("");

            $isHit = GameController::checkIsHit(self::$enemyFleet, self::parsePosition($position));
            if ($isHit) {
                self::printHitImage();
            }

            $isHit
                ? self::$console->printHit()
                : self::$console->printMiss()
            ;

            self::$console->println();
            self::$console->printDividerLine();

            $position = self::getRandomPosition();
            $isHit = GameController::checkIsHit(self::$myFleet, $position);
            self::$console->println();
            self::$console->println(
                sprintf('Computer shoot in Column [%s] and Row [%s]', $position->getColumn(), $position->getRow())
            );
            $isHit
                ? self::$console->printHit()
                : self::$console->printMiss()
            ;
            if ($isHit) {
                self::printHitImage();
            }

//            exit();
        }
    }

    public static function printHitImage(): void
    {
        self::beep();

        self::$console->println("                \\         .  ./");
        self::$console->println("              \\      .:\" \";'.:..\" \"   /");
        self::$console->println("                  (M^^.^~~:.'\" \").");
        self::$console->println("            -   (/  .    . . \\ \\)  -");
        self::$console->println("               ((| :. ~ ^  :. .|))");
        self::$console->println("            -   (\\- |  \\ /  |  /)  -");
        self::$console->println("                 -\\  \\     /  /-");
        self::$console->println("                   \\  \\   /  /");
    }

    /**
     * @throws Exception
     */
    public static function parsePosition($input): Position
    {
        $letter = substr($input, 0, 1);
        $number = substr($input, 1, 1);

        if (!is_numeric($number)) {
            throw new Exception("Not a number: $number");
        }

        return new Position($letter, $number);
    }
}
